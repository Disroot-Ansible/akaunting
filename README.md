# Akaunting - Ansible Role
This role covers deployment, configuration and software updates of Akaunting. This role is released under MIT Licence and we give no warranty for this piece of software. Currently supported OS - Debian.

You can deploy test instance using `Vagrantfile` attached to the role.
`vagrant up`

`ansible-playbook -b Playbooks/akaunting.yml`

Then you can access Akaunting from your computer on http://192.168.33.31


## Playbook
The playbook includes mariadb, php-fpm and nginx roles and deploys entire stack needed to run Akaunting. Additional roles are also available in the Ansible roles repos in git.


## CHANGELOG
  - **07.04.2021** - Create the role
